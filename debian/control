Source: valadoc
Section: devel
Priority: extra
Maintainer: Sebastian Reichel <sre@debian.org>
Build-Depends: debhelper (>= 10~),
               libgee-0.8-dev (>= 0.19.91),
               libglib2.0-dev (>= 2.24.0),
               libgraphviz-dev (>= 2.16),
               libgtk2.0-dev (>= 2.10.0),
# add build dependencies for each vala version,
# which should be supported by valadoc
               libvala-0.36-dev,
               valac (>= 0.36~)
Standards-Version: 3.9.8
Homepage: http://www.valadoc.org
Vcs-Browser: https://salsa.debian.org/gnome-team/valadoc
Vcs-Git: https://salsa.debian.org/gnome-team/valadoc.git

Package: valadoc
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: valac
Multi-Arch: foreign
Description: API documentation generator for vala
 Valadoc is a documentation generator for generating API documentation
 in HTML format from Vala source code. It can be used for *.vala and
 *.vapi files.

Package: libvaladoc3
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libvaladoc-data
Description: API documentation generator for vala (library)
 Valadoc is a documentation generator for generating API documentation
 in HTML format from Vala source code. It can be used for *.vala and
 *.vapi files.
 .
 This package contains the library used by valadoc.

Package: libvaladoc-data
Section: misc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Replaces: valadoc (<< 0.3.2~git20130319-1)
Breaks: valadoc (<< 0.3.2~git20130319-1)
Description: API documentation generator for vala (data)
 Valadoc is a documentation generator for generating API documentation
 in HTML format from Vala source code. It can be used for *.vala and
 *.vapi files.
 .
 This package contains the icons used by valadoc.

Package: libvaladoc-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         libgee-0.8-dev (>= 0.8),
         libvaladoc3 (= ${binary:Version}),
         libgraphviz-dev (>= 2.16)
Description: API documentation generator for vala (devel files)
 Valadoc is a documentation generator for generating API documentation
 in HTML format from Vala source code. It can be used for *.vala and
 *.vapi files.
 .
 This package contains the devel files for the valdoc library.
